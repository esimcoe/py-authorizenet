# -*- coding: utf-8 -*-


###############################################################################
#
#  Errors
#
###############################################################################
class ExecutionError(RuntimeError):
    pass

class CommunicationError(ExecutionError):
    pass

class NoResponseError(ExecutionError):
    pass

class InvalidResponseError(ExecutionError):
    pass

class DocumentCreationError(ExecutionError):
    pass
